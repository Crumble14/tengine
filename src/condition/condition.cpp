#include "condition.hpp"
#include "../tengine.hpp"

using namespace TEngine;

void Condition::init_vm()
{
	// TODO Load natives
	vm.bind_program(program.program);
}

bool Condition::check_row(const Row& row, const Table& table)
{
	vm.prepare();

	Thread thread(pthread_self());
	auto& stack = thread.get_stack();

	bool* result = new bool;
	stack.push(result);

	for(size_t i = program.fields.size(); i >= 1; --i) {
		const auto& f = program.fields[i - 1];
		const auto& c = table.get_column(f);

		if(!c) {
			throw invalid_argument(string("Column `") + f + "` doesn't exist!");
		}

		if(c->get_type() == STRING_T) {
			// TODO
		} else if(c->get_type() == CLOB_T) {
			// TODO
		} else {
			stack.push(row.get<int32_t>(*c));
		}
	}

	vm.run(0, thread);

	bool r = *result;
	delete result;
	return r;
}
