#ifndef CONDITION_HPP
# define CONDITION_HPP

# include<string>
# include<vector>

# include<tungsten.hpp>

namespace TEngine
{
	using namespace std;
	using namespace Tungsten;

	struct ProgramData
	{
		string str;
		Program program;

		vector<string> fields;

		ProgramData() = default;

		inline ProgramData(const string& str)
			: str{str}
		{}
	};

	enum Operator : char
	{
		ADD_O = '+',
		SUBTRACT_O = '-',
		MULTIPLY_O = '*',
		DIVIDE_O = '/',
		MODULO_O = '%',

		UNKNOWN_O = '?'
	};

	const vector<Operator> operators = {
		ADD_O, SUBTRACT_O,
		MULTIPLY_O, DIVIDE_O, MODULO_O
	};

	typedef const char* Comparator;

	constexpr auto EQUAL_C = "==";
	constexpr auto NOT_EQUAL_C = "!=";
	constexpr auto LOWER_C = "<";
	constexpr auto LOWER_OR_EQUAL_C = "<=";
	constexpr auto HIGHER_C = ">";
	constexpr auto HIGHER_OR_EQUAL_C = ">=";

	constexpr auto AND_C = "&&";
	constexpr auto OR_C = "||";

	constexpr auto UNKNOWN_C = "?";

	const unordered_map<Comparator, Opcode> comparators = {
		{EQUAL_C, EQ},
		{NOT_EQUAL_C, NEQ},
		{LOWER_C, LOW},
		{LOWER_OR_EQUAL_C, LOWE},
		{HIGHER_C, HIGH},
		{HIGHER_OR_EQUAL_C, HIGHE},
		{AND_C, AND},
		{OR_C, OR}
	};

	void skip_spaces(string& str);
	Operator get_operator(string& str);
	Comparator get_comparator(string& str);
	bool is_valid_number(const string& str);
	bool is_valid_fieldName(const string& str);
	bool is_float(const string& str);
	int atoi(string& str);
	float atof(string& str);
	string get_fieldName(string& str, const bool peek);
	Opcode op_to_opcode(const Operator op);
	Opcode comp_to_opcode(const Comparator co);

	ProgramData compile_condition(const string& condition);

	class Row;
	class Table;

	class Condition
	{
		public:
			Condition() = default;

			inline Condition(const string& str)
			{
				program = compile_condition(str);
				init_vm();
			}

			inline const string& get_string() const
			{
				return program.str;
			}

			inline const vector<string>& get_fields() const
			{
				return program.fields;
			}

			bool check_row(const Row& row, const Table& table);

		private:
			ProgramData program;
			VM vm;

			void init_vm();
	};
}

#endif
