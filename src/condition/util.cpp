#include "condition.hpp"

using namespace TEngine;

void TEngine::skip_spaces(string& str)
{
	while(!str.empty()
		&& (str.front() == ' ' || str.front() == '\t')) {
		str.erase(0, 1);
	}
}

Operator TEngine::get_operator(string& str)
{
	for(const auto& o : operators) {
		if(o == str.front()) {
			return o;
		}
	}

	return UNKNOWN_O;
}

Comparator TEngine::get_comparator(string& str)
{
	for(const auto& c : comparators) {
		const auto& s = c.first;

		// TODO Bug fix: < will be found when <= is given
		if(str.find(s) == 0) {
			str.erase(0, strlen(s));
			return s;
		}
	}

	return UNKNOWN_C;
}

bool TEngine::is_valid_number(const string& str)
{
	size_t i = 0;

	if(str[i] == '-') ++i;
	return ((str[i] >= '0' && str[i] <= '9') || str[i] == '.');
}

bool TEngine::is_valid_fieldName(const string& str)
{
	if(str.empty()) return false;
	return ((str.front() >= 'a' && str.front() <= 'z')
		|| (str.front() >= 'A' && str.front() <= 'Z')
		|| str.front() == '_');
}

bool TEngine::is_float(const string& str)
{
	size_t i = 0;

	while((str[i] >= '0' && str[i] <= '9')
		|| str[i] == '.' || str[i] == '-') {
		if(str[i] == '.') return true;
		++i;
	}

	return false;
}

int TEngine::atoi(string& str)
{
	size_t i = 0;

	while(str[i] <= 32 || str[i] == 127) ++i;

	int sign = (str[i] == '-' ? -1 : 1);
	if(str[i] == '+' || str[i] == '-') ++i;

	int n = 0;

	while(str[i] >= '0' && str[i] <= '9') {
		n *= 10;
		n += str[i] - '0';

		++i;
	}

	str.erase(0, i);
	return (sign * n);
}

float TEngine::atof(string& str)
{
	(void) str;
	// TODO

	return 0;
}

string TEngine::get_fieldName(string& str, const bool peek)
{
	if(str.empty()) return str;

	string name;
	size_t i = 0;

	while(i < str.size()
		&& ((str[i] >= 'a' && str[i] <= 'z')
		|| (str[i] >= 'A' && str[i] <= 'Z')
		|| str[i] == '_')) {
		name += str[i];
		++i;
	}

	if(!peek) str.erase(0, i);
	return name;
}

Opcode TEngine::op_to_opcode(const Operator op)
{
	switch(op) {
		case ADD_O: {
			return ADD;
		}

		case SUBTRACT_O: {
			return SUB;
		}

		case MULTIPLY_O: {
			return MUL;
		}

		case DIVIDE_O: {
			return DIV;
		}

		case MODULO_O: {
			return MOD;
		}

		default: {
			throw invalid_argument("Unknown operator!");
		}
	}
}

Opcode TEngine::comp_to_opcode(const Comparator co)
{
	const auto c = comparators.find(co);

	if(c != comparators.cend()) {
		return c->second;
	}

	throw runtime_error("Unknown error!");
}
