#include "condition.hpp"

using namespace TEngine;

void parse_comparisons(ProgramData& data);

void parse_number(ProgramData& data)
{
	skip_spaces(data.str);

	if(data.str.empty()) {
		throw invalid_argument("Expected operand!");
	}

	if(data.str.front() == '(') {
		data.str.erase(0, 1);
		skip_spaces(data.str);

		parse_comparisons(data);

		if(data.str.front() == ')') {
			data.str.erase(0, 1);
		}
	} else if(is_valid_number(data.str)) {
		const auto i = (is_float(data.str)
			? atof(data.str) : atoi(data.str));
		data.program.instructions.emplace_back(PUSH, 4, i);
	} else if(is_valid_fieldName(data.str)) {
		const auto name = get_fieldName(data.str, false);

		for(size_t i = 0; i < data.fields.size(); ++i) {
			const auto& f = data.fields[i];

			if(f == name) {
				data.program.instructions.emplace_back(VLOAD, 4, (i << 8) | 32);
				break;
			}
		}
	} else {
		throw invalid_argument("Invalid operand!");
	}
}

void parse_factors(ProgramData& data)
{
	parse_number(data);

	while(true) {
		skip_spaces(data.str);
		if(data.str.empty() || data.str.front() == ')') break;

		const auto op = get_operator(data.str);
		if(op != MULTIPLY_O && op != DIVIDE_O && op != MODULO_O) break;

		data.str.erase(0, 1);
		skip_spaces(data.str);

		if(data.str.empty()) {
			throw invalid_argument("Expected right operand for factor!");
		}

		parse_number(data);

		data.program.instructions.emplace_back(op_to_opcode(op), 4, 0);
	}
}

void parse_sums(ProgramData& data)
{
	parse_factors(data);

	while(true) {
		skip_spaces(data.str);
		if(data.str.empty() || data.str.front() == ')') break;

		const auto op = get_operator(data.str);
		if(op != ADD_O && op != SUBTRACT_O) break;

		data.str.erase(0, 1);
		skip_spaces(data.str);

		if(data.str.empty()) {
			throw invalid_argument("Expected right operand for sum!");
		}

		parse_factors(data);

		data.program.instructions.emplace_back(op_to_opcode(op), 4, 0);
	}
}

void parse_comparisons(ProgramData& data)
{
	parse_sums(data);

	while(true) {
		skip_spaces(data.str);
		if(data.str.empty()) break;

		const auto co = get_comparator(data.str);
		if(co == UNKNOWN_C) break;

		skip_spaces(data.str);

		if(data.str.empty()) {
			throw invalid_argument("Expected right operand for comparison!");
		}

		parse_sums(data);

		data.program.instructions.emplace_back(comp_to_opcode(co), 4, 0);
	}
}

void find_fields(ProgramData& data)
{
	size_t i = 0;

	while(i < data.str.size()) {
		string str(data.str.cbegin() + i, data.str.cend());
		const auto name = get_fieldName(str, true);

		if(!name.empty()) {
			const auto args = (data.fields.size() << 8) | 32;
			data.program.instructions.emplace_back(VCREATE, 4, 0);
			data.program.instructions.emplace_back(VSTORE, 4, args);

			data.fields.push_back(name);
			i += name.size();
		} else {
			++i;
		}
	}
}

ProgramData TEngine::compile_condition(const string& condition)
{
	ProgramData data(condition);

	auto& insts = data.program.instructions;
	insts.emplace_back(CALL, 4, 1);

	find_fields(data);
	parse_comparisons(data);

	if(insts.back().opcode == VLOAD) {
		insts.emplace_back(POP, 2, 0);
		insts.emplace_back(POP, 1, 0);
	}

	insts.emplace_back(MSTORE, 1, 0);

	if(!data.str.empty()) {
		throw invalid_argument("Invalid expression!");
	}

	return data;
}
