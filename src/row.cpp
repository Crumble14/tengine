#include "tengine.hpp"

using namespace TEngine;

string Row::get_clob(const Column& column) const
{
	if(column.get_type() != CLOB_T) {
		throw invalid_argument("Column type is not clob!");
	}

	const auto p = data + table->get_column_begin(column);
	clob_pos_t pos;
	memcpy(&pos, p, column.get_size());

	const auto val = table->read_clob(pos);
	const string value(val);
	free(val);

	return value;
}

void Row::set_clob(const Column& column, const string& value)
{
	if(column.get_type() != CLOB_T) {
		throw invalid_argument("Column type is not clob!");
	}

	const auto p = data + table->get_column_begin(column);
	clob_pos_t pos;
	memcpy(&pos, p, column.get_size());

	table->write_clob(pos, value.data());
	memcpy(p, &pos, column.get_size());
}
