#include "tengine.hpp"

using namespace TEngine;

template<typename T>
T Row::get(const Column& column) const
{
	const auto p = data + table->get_column_begin(column);
	T value;
	memcpy(&value, p, column.get_size());

	return value;
}

template<typename T>
void Row::set(const Column& column, const T& value)
{
	const auto p = data + table->get_column_begin(column);
	memcpy(p, &value, column.get_size());
}
